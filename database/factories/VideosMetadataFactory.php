<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */
use App\Videos_meta;
use Illuminate\Support\Str;
use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(App\Videos_meta::class, function (Faker $faker) {
    return [
        'video_id' => $faker->text,
        'video_size' => $faker->text,
        'viewers_count' => $faker->text,
    ];
});
