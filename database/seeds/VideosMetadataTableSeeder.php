<?php

use Illuminate\Database\Seeder;

class VideosMetadataTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Videos_meta::class, 7)->create();
    }
}
