<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVideosMetasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::enableForeignKeyConstraints();
        Schema::create('videos_metas', function (Blueprint $table) {
            $table->increments('id');
            $table->string('video_id')->nullable();
            $table->foreign('video_id')->references('video_id')->on('videos');
            $table->string('video_size');
            $table->string('viewers_count');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('videos_metas');
    }
}
