<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('login', 'UserController@login');
Route::post('register', 'API\UserController@register');
Route::group(['middleware' => 'auth:api'], function(){
	Route::post('details', 'API\UserController@details');
});

// get specific video size
Route::apiResource('videos', 'VideosController');
Route::get('videosize/{un}','VideosController@videosize');
// get specific video meta
Route::get('videometadata/{vid}','VideosController@videometadata');
// update video
Route::put('videoupdate/{vid}','VideosController@update');