<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class Videos_metas extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        //return parent::toArray($request);
        return [
            'id' => $this->id,
            'video_id' => $this->video_id,
            'video_size' => $this->video_size,
            'viewers_count' => $this->viewers_count
        ];
    }
}
