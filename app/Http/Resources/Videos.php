<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class Videos extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        //return parent::toArray($request);

         return [
            'id' => $this->id,
            'video_id' => $this->video_id,
            'created_by' => $this->created_by
        ];
    }
}
