<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Videos;
use App\Http\Resources\Videos as VideosResource;

class VideosController extends Controller
{

    public $successStatus = 200;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Videos::all();

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
            
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        /*$task = VideosMetas::where('video_id', $request->input('video_id'))->update(['video_size' => $request->input('video_size'), 'viewers_count' => $request->input('viewers_count')]);
 
        if($task) {
            return new VideosResource($task);
        } */

        $resource = VideosMetas::findOrFail($id);
        $resource->fill($request->only(['video_size', 'viewers_count']));
        $resource->save();

        if($resource->save()) {
            return new TaskResource($resource);
        } 
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /** 
     * return Video size api 
     * 
     * @return \Illuminate\Http\Response 
     */ 
    public function videosize($un) 
    { 
        
        try {
            
            $task = Videos::where('created_by', $un)
                    ->leftJoin('videos_metas', 'videos_metas.video_id', '=', 'videos.video_id')
                    ->get();

            if( !empty($task) ){

                $total_video_size = 0;
                foreach ($task as $value) {
                    $total_video_size += $value['video_size'];
                }

                return response()->json(['total_video_size' => $total_video_size.' MB'], $this->successStatus);
            }  else {

                return response()->json(['error' => 'Oops! User not found'], 403); 

            }   
             

        } catch (RequestException $ex) {

            return response()->json(['error' => $ex.' External API call failed.'], 500); 
        }

      
 
    } 

    /** 
     * return Video size api 
     * 
     * @return \Illuminate\Http\Response 
     */ 
    public function videometadata($vid) 
    { 
        try {
            
            $task = Videos::leftJoin('videos_metas', 'videos_metas.video_id', '=', 'videos.video_id')
                    ->select('video_size', 'viewers_count', 'created_by')
                    ->where('videos.video_id', $vid)
                    ->first();

            if( !empty($task) ){

                return response()->json([ 'success' => $task], $this->successStatus);

            }  else {

                return response()->json(['error' => 'Oops! Video not found'], 403); 

            }   
             

        } catch (RequestException $ex) {

            return response()->json(['error' => $ex.' External API call failed.'], 500); 
        }

    } 


}
